import 'package:shopping/models/category_item.dart';
import 'package:shopping/models/category.dart';
import 'package:shopping/data/categories.dart';

final groceryItems = [
  GroceryItem(id: 'a', name: 'Leche', quantity: 1, category: categories[Categories.dairy]!),
  GroceryItem(id: 'b', name: 'Platanos', quantity: 5, category: categories[Categories.fruit]!),
  GroceryItem(id: 'c', name: 'Carnes rojas', quantity: 1, category: categories[Categories.meat]!),
];
