class DummyJson {
  static String getJsonString() {
    const jsonString = '''
      [
        {"id": "a", "name": "manzanas", "quantity": 5, "category": "fruit"},
        {"id": "b", "name": "pan", "quantity": 1, "category": "carbs"},
        {"id": "c", "name": "leche", "quantity": 2, "category": "dairy"}
      ]
    ''';
    return jsonString;
  }
}
