import 'package:flutter/material.dart';
import 'package:shopping/data/dummy_items.dart';
import 'package:shopping/models/category_itemDAO.dart';
import 'package:shopping/models/either_category_item.dart';
import 'package:shopping/widgets/new_item.dart';
import 'package:shopping/services/category_items_service.dart';

class GroceryList extends StatefulWidget {
  const GroceryList({super.key});

  @override
  State<GroceryList> createState() => _GroceryListState();
}

class _GroceryListState extends State<GroceryList> {
  void _addItem() {
    Navigator.of(context).push(
      MaterialPageRoute(
        builder: (ctx) => const NewItem(),
      ),
    );
  }

  CategoryItemsService categoryItemsService = CategoryItemsService();

  @override
  Widget build(BuildContext context) {
    final itemsDTO = categoryItemsService.checkGroceryItemsFromJson(groceryItems);

    return Scaffold(
      appBar: AppBar(
        title: const Text('Tus productos'),
        actions: [
          IconButton(
            icon: const Icon(Icons.add),
            onPressed: _addItem,
          ),
        ],
      ),
      body: itemsDTO.isRight()
          ? ListView.builder(
              itemCount: itemsDTO.right!.length,
              itemBuilder: (context, index) => ListTile(
                title: Text(itemsDTO.right![index].name),
                leading: Container(
                  width: 24,
                  height: 24,
                  color: itemsDTO.right![index].category.color,
                ),
                trailing: Text(
                  itemsDTO.right![index].quantity.toString(),
                ),
              ),
            )
          : Center(
              child: Text(
                itemsDTO.left.toString(),
              ),
            ),
    );
  }
}
