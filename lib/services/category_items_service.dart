import 'package:shopping/models/either_category_item.dart';
import 'package:shopping/models/category_item.dart';
import 'package:shopping/models/category_itemDAO.dart';
import 'package:shopping/data/dummy_json.dart';
import 'dart:convert';

//import 'package:shopping/data/dummy_items.dart';

class CategoryItemsService {
  EitherCategoryItem checkGroceryItemsFromJson(List<GroceryItem> groceryItems) {
    final List<dynamic> jsonList = jsonDecode(DummyJson.getJsonString());
    final List<GroceryItem> items = jsonList.map((e) => GroceryItem.fromJson(e)).toList();
    final List<GroceryItemDAO> itemsDTO = convertGroceryItemToDTO(items);

    if (itemsDTO.isEmpty) {
      return EitherCategoryItem.left(Exception('Productos no encontrados'));
    } else {
      return EitherCategoryItem.right(items);
    }
  }

  List<GroceryItemDAO> convertGroceryItemToDTO(List<GroceryItem> items) {
    List<GroceryItemDAO> itemsDTO = [];
    for (var item in items) {
      itemsDTO.add(GroceryItemDAO(
          // id: int.parse(item.id), name: item.name, quantity: item.quantity, category: item.category.toString()));
          id: item.id,
          name: item.name,
          quantity: item.quantity,
          //category: item.category.toString()));
          category: item.category));
    }
    return itemsDTO;
  }
}

  // EitherCategoryItem _dummyleft() {
  //   return EitherCategoryItem.left(Exception('Productos no encontrados'));
  // }

  // EitherCategoryItem _dummyRight() {
  //   return EitherCategoryItem.right(groceryItems);
  // }

