import 'package:shopping/models/category_item.dart';
import 'package:shopping/services/category_items_service.dart';

class EitherCategoryItem {
  EitherCategoryItem.right(List<GroceryItem> items) {
    _left = null;
    _right = items;
  }

  EitherCategoryItem.left(Exception exception) {
    _left = exception;
    _right = null;
  }

  Exception? _left;
  List<GroceryItem>? _right;

  Exception? get left => _left;
  List<GroceryItem>? get right => _right;

  bool isLeft() {
    if (_left != null && _right == null) {
      return true;
    } else {
      return false;
    }
  }

  bool isRight() {
    if (_left == null && _right != null) {
      return true;
    } else {
      return false;
    }
  }
}
