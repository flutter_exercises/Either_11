import 'package:shopping/models/category.dart';
import 'package:shopping/data/categories.dart';

class GroceryItem {
  const GroceryItem({
    required this.id,
    required this.name,
    required this.quantity,
    required this.category,
  });

  final String id;
  final String name;
  final int quantity;
  final Category category;

  factory GroceryItem.fromJson(Map<String, dynamic> json) {
    return GroceryItem(
        id: json['id'], name: json['name'], quantity: json['quantity'], category: _parseCategory(json['category']));
  }

  static Category _parseCategory(String categoryString) {
    switch (categoryString) {
      case 'fruit':
        return categories[Categories.fruit]!;
      case 'vegetables':
        return categories[Categories.vegetables]!;
      case 'dairy':
        return categories[Categories.dairy]!;
      case 'carbs':
        return categories[Categories.carbs]!;
      case 'meat':
        return categories[Categories.meat]!;
      case 'sweets':
        return categories[Categories.sweets]!;
      case 'spices':
        return categories[Categories.spices]!;
      case 'convenience':
        return categories[Categories.convenience]!;
      case 'hygiene':
        return categories[Categories.hygiene]!;
      default:
        return categories[Categories.other]!;
    }
  }
}
