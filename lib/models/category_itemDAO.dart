import 'package:shopping/models/category.dart';
import 'package:shopping/data/categories.dart';

class GroceryItemDAO {
  const GroceryItemDAO({required this.id, required this.name, required this.quantity, required this.category});
  final String id;
  final String name;
  final int quantity;
  final Category category;

  factory GroceryItemDAO.fromJson(Map<String, dynamic> json) {
    return GroceryItemDAO(id: json['id'], name: json['name'], quantity: json['quantity'], category: json['category']);
  }

  Map<String, dynamic> toJson() => {'id': id, 'name': name, 'quantity': quantity, 'category': category};
}
